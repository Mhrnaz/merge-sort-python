import random
import time

last_ten_output=[]


def merge_sort(sort_list):
    list_size = int(len(sort_list)/2)
    if len(sort_list) == 1:
        return sort_list
    left_slice = list(sort_list[:list_size])
    right_slice = list(sort_list[list_size:])
    left_list = merge_sort(left_slice)
    right_list = merge_sort(right_slice)
    # print("left list before sort:"+str(sort_list[:list_size]))
    # print("right list before sort"+str(sort_list[list_size:]))
    # print("left list after sort:"+str(left_list))
    # print("right list after sort:"+str(right_list))
    result = merge(left_list, right_list)
    return result


def merge(left_list, right_list):
    result = []
    left_size = len(left_list)
    right_size = len(right_list)
    left_pointer = 0
    right_pointer = 0
    while left_pointer < left_size and right_pointer < right_size:
        if left_list[left_pointer] <= right_list[right_pointer]:
            result.append(left_list[left_pointer])
            left_pointer += 1
        else:
            result.append(right_list[right_pointer])
            right_pointer += 1
    if left_pointer == left_size:
        while right_pointer < right_size:
            result.append(right_list[right_pointer])
            right_pointer += 1
    else:
        while left_pointer < left_size:
            result.append(left_list[left_pointer])
            left_pointer += 1
    return result


def shift_list(shifting_list):
    for i in range(1,len(shifting_list)):
        shifting_list[i-1]=shifting_list[i]
    return shifting_list


def generate_random_number():
    r = random.randint(1, 1000000)
    while last_ten_output.__contains__(r):
        r = random.randint(1, 1000000)
    if len(last_ten_output) < 10:
        last_ten_output.append(r)
    else:
        shift_list(last_ten_output)
        last_ten_output[9] = r
    return r


numbers = []
random_checker = []
size = 100
for i in range(size):
    # rand = random.randint(0,size*10)
    # if !num
    numbers.append(random.randint(0, size*10))
    random_checker.append(generate_random_number())

print(random_checker)
print("")
print("")
millis1 = int(round(time.time() * 1000000000))
sorted_list = merge_sort(numbers)
millis2 = int(round(time.time() * 1000000000))
print(millis2-millis1)
print(sorted_list)